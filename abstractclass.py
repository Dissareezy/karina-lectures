from abc import ABC, abstractmethod

class Shape(ABC): # Абстрактный класс - интерфейс
    @abstractmethod
    def area(self):
        pass

    @abstractmethod
    def perimetr(self):
        pass

class Rectangle(Shape): # НЕ унаследование , а имплементация
    def __init__(self,width,height):
        self.width = width
        self.height = height

    def area(self):
        return self.width * self.height

    def perimetr(self):
        return 2 * (self.width + self.height)

