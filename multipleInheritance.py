class Father: #Parent A
    def __init__(self, hair_color):
        self.hair_color = hair_color

class Mother: #Parent B
    def __init__(self, eyes_color):
        self.eyes_color = eyes_color

class Child(Father,Mother): #Child A + B
    def __init__(self,hair_color,eyes_color,height):
        Father.__init__(self,hair_color)
        Mother.__init__(self,eyes_color)
        self.height = height

