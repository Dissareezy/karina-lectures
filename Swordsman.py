#Inheritance ( наследование )
from second import Human

class Swordsman(Human): # в скобки мы должны передать класс от которого мы что-либо унаследовали.
    def Attack(self): # overriding метода который был в родительском классе
        return print("Attacking with a sword!")

    def WhoAmI(self):
        return print("I am Swordsman!!!")

Timur = Swordsman("Smagulov","Timur")

Timur.SetAge(12)
