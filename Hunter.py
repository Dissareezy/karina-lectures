from second import Human
from Swordsman import Timur

class Hunter(Human):
    pass

Katya = Hunter("Windows","Ekaterina")

#Polymorphysm ( Полиморфизм ) Это когда один метод на двоих ?
print(Katya.Talk("Ya Katya"))
print(Timur.Talk("A ya Timur!"))

Timur.Attack()
Katya.Attack()