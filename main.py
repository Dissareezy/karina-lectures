
class MyClass: # Название класса
    attribute1 = 15         # Атрибуты
    attribute2 = "string123123"

    def __init__(self, attribute1): # Конструктор
        self.attribute1 = attribute1

    def Method1(self): # Метод класса
        return print(self.attribute2)


Object1 = MyClass(123123) # Создание обьекта
Object1.Method1()


