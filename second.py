#DRY
#DONT REPEAT YOURSELF!
class Human():
    #attributes
    __age = 0

    def Attack(self):
        print("ATTACKS!!!")

    def __init__(self,l_name,f_name): #Constructor
        self.__l_name = l_name
        self.__f_name = f_name

    def Talk(self,message): #Method q
        return message

    def __Sleep(self):
        return print("Ilyas is sleeping")

    # Getters and Setters
    def GetAge(self):
        return self.__age

    def Get_l_name(self):
        return self.__l_name

    def Get_f_name(self):
        return self.__f_name

    def SetAge(self,age):
        if age < 0:
            print("Age cannot be negative")
        else:
            self.__age = age

    def Set_l_name(self,l_name):
        self.__l_name = l_name

    def Set_f_name(self,f_name):
        self.__f_name = f_name
    #end

Ilyas = Human("Pythonovich", "Ilyas")
Ilyas.SetAge(10)